#include <iostream>

int main() {
    int num1, num2;

    std::cout << "Enter two numbers: ";
    std::cin >> num1 >> num2;

    if (num1 > num2) {
        int temp = num1;
	num1 = num2;
	num2 = temp;
    }
    
    int sum = 0;
    for (int i = num1; i <= num2; i++) sum += i;
    
    std::cout << "Sum from "
              << num1 << " to "
              << num2 << " is "
              << sum << std::endl;
}
