#include <time.h>

#include <iostream>
#include <vector>


int main() {
    int min = INT8_MAX,
        max = INT8_MIN,
        count = 10;
    
    srand(time(NULL));

    int numbers[count];

    std::cout << "Random integers: ";
    
    for (int i = 0; i < count; i++) {
      numbers[i] = rand() % 100 + 1;
      std::cout << numbers[i] << " ";
      if (numbers[i] > max) max = numbers[i];
      if (numbers[i] < min) min = numbers[i];
    }

    std::cout << "\nMinimum is "
              << min
              << ", maximum is "
              << max
              << std::endl;
}
