#include <iostream>

int main() {
    int num;

    std::cout << "Enter an integer: ";
    std::cin >> num;

    std::cout << "Primes to " << num << " are: ";
    
    for (int i = 2; i <= num; i++) {
        bool is_prime = true;
      
	for (int j = 2; j <= i / 2; j++)
	    if (i % j == 0) is_prime = false;

	if (is_prime) std::cout << i << " ";
    }
    
    std::cout << std::endl;
}
