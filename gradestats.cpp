#include <iostream>
#include <vector>

int main() {
    std::vector<float> grades;
    float grade, sum, average;

    while (grade != -1) {
        std::cout << "Enter grade (or -1 to end): ";
        std::cin >> grade;

	if (grade == -1 || grade < 0 || grade > 100) {
	  if (grade == -1) break;
	  
	  /* warn the user they entered 
             an incorrect number & reset 
             the loop:
          */
	  std::cout << "Please make sure your grade is between 0 and 100"
		    << std::endl;
	  continue;
	}

        grades.push_back(grade);
    }
    
    for (float num : grades) sum += num;
    
    average = sum / grades.size();

    std::cout << "\nThe average is: " << average << std::endl;

    std::cout << "The grades are: ";
    for (float num : grades) {
      std::cout << " " << num << ", ";
    }
}

